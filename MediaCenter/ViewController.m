//
//  ViewController.m
//  MediaCenter
//
//  Created by Alienchang on 2016/12/5.
//  Copyright © 2016年 Alienchang. All rights reserved.
//

#import "ViewController.h"
#import "FileManager.h"
#import "ImageHelper.h"
#import "MediaPickerViewController.h"
#import "MediaNavigationViewController.h"
@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [[FileManager manager] createMediaFolder];
    if (![ImageHelper isOpenAuthority]) {
        
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"您未打开照片权限" message:nil preferredStyle:UIAlertControllerStyleAlert];
        [alertController addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            ;
        }]];
        [alertController addAction:[UIAlertAction actionWithTitle:@"去设置" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [ImageHelper jumpToSetting];
        }]];
        
        [self presentViewController:alertController animated:YES completion:nil];
        
        return;
    }
    [ImageHelper getAlbumList:^(NSArray<PHFetchResult *> *albumList) {
        NSLog(@"albumList == %@",albumList);
        PHFetchResult *fetchResult = albumList[0];
        NSLog(@"fetchResult == %@",fetchResult);
        [ImageHelper getImageDataWithAsset:fetchResult[0] complete:^(UIImage *image) {
            NSLog(@"image == %@",image);
        }];
    }];
    
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(100, 300, 50, 50)];
    [button setBackgroundColor:[UIColor blackColor]];
    [button addTarget:self action:@selector(buttonAction) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:button];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)buttonAction{
    MediaNavigationViewController *mediaNavigationViewController = [MediaNavigationViewController new];
    [self presentViewController:mediaNavigationViewController animated:YES completion:nil];

}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
