//
//  FileManager.m
//  MediaCenter
//
//  Created by Alienchang on 2016/12/5.
//  Copyright © 2016年 Alienchang. All rights reserved.
//

#import "FileManager.h"
#import "ImageHelper.h"
#import "NSDictionary+Extension.h"
#import "EGOCache.h"
#import "YTKKeyValueStore.h"

NSString * const kAlbums     = @"albums";
NSString * const kAlbumInfo  = @"albumInfo.json";
NSString * const kAlbumTable = @"kAlbumTable";
NSString * const kDBName     = @"MediaCenterDB";
@interface FileManager(){
    NSInteger _imageNum;
}
@end
@implementation FileManager

+ (instancetype)manager{
    static FileManager *manager;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager     = [FileManager new];
    });
    return manager;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        _imageNum   = 0;
    }
    return self;
}
- (void)createMediaFolder{
    NSFileManager   *fileManager    = [NSFileManager defaultManager];
    NSString        *pathDocuments  = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString        *createDir      = [NSString stringWithFormat:@"%@/Medias", pathDocuments];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:createDir]) {
        [fileManager createDirectoryAtPath:createDir withIntermediateDirectories:YES attributes:nil error:nil];
    } else {
        NSLog(@"FileDir is exists.");
    }
}

- (BOOL)writeImage:(UIImage *)image toPath:(NSString *)path{
    return YES;
}
- (BOOL)writeAsset:(PHAsset *)asset toPath:(NSString *)path{

    NSString        *pathDocuments  = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString        *targetPath     = [NSString stringWithFormat:@"%@/Medias/%@", pathDocuments,path];
    
    //如果是图片
    if (asset.mediaType == PHAssetMediaTypeImage) {
        [ImageHelper getImageDataWithAsset:asset complete:^(UIImage *image) {
            _imageNum ++;
            NSTimeInterval interval = [[NSDate date] timeIntervalSince1970];
            interval += _imageNum;
            NSString *wholeImageName= [NSString stringWithFormat:@"%@/%ld.jpg",targetPath,(long)(interval * 1000)];
            NSData   *imageData     = UIImagePNGRepresentation(image);
            
            BOOL save = [imageData writeToFile:wholeImageName atomically:NO];
            NSLog(@"%@储存%@",wholeImageName,save?@"成功":@"失败");
        }];
    }
    
    return YES;
}
#pragma mark --public func
- (AlbumModel *)albumWithName:(NSString *)name{
    YTKKeyValueStore *store = [self appKeyValueStore];
    NSString *tableName = kAlbumTable;
    NSDictionary *albumDic = [store getObjectById:name fromTable:tableName];
    AlbumModel *albumModel = [AlbumModel mj_objectWithKeyValues:albumDic];
    return albumModel;
}

- (NSArray <AlbumModel *>*)albums{
    YTKKeyValueStore *store = [self appKeyValueStore];
    NSString *tableName = kAlbumTable;
    NSArray <YTKKeyValueItem *>*keyValueItems = [store getAllItemsFromTable:tableName];
    NSArray *albums = [AlbumModel mj_objectArrayWithKeyValuesArray:[keyValueItems valueForKeyPath:@"@unionOfObjects.itemObject"]];
    return albums;
}

- (BOOL)createAlbum:(AlbumModel *)albumModel {
    
    if (!albumModel || !albumModel.albumName) {
        return NO;
    }
    NSString *albumsPath = [NSString stringWithFormat:@"%@/%@",[self albumsPath],albumModel.albumName];
    if ([self createFolderAtPath:albumsPath]) {
        NSDictionary *albumDic = [albumModel mj_keyValues];
        [self writeContent:[NSDictionary convertToJSONData:albumDic] toFilePath:[NSString stringWithFormat:@"%@/%@",albumsPath,kAlbumInfo]];
        
        YTKKeyValueStore *store = [self appKeyValueStore];
        NSString *tableName = kAlbumTable;
        [store createTableWithName:tableName];
        [store putObject:albumDic withId:albumModel.albumName intoTable:tableName];
        return YES;
    }else{
        return NO;
    }
}

#pragma mark --private func
- (YTKKeyValueStore *)appKeyValueStore{
    YTKKeyValueStore *store = [[YTKKeyValueStore alloc] initDBWithName:kDBName];
    return store;
}
- (void)writeContent:(NSString *)content toFilePath:(NSString *)filePath {
    [content writeToFile:filePath atomically:YES
                encoding:NSUTF8StringEncoding error:nil];
}

- (NSString *)readFileFromFilePath:(NSString *)filePath{
    NSString *content = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:nil];
    return content;
}

- (NSString *)albumsPath {
    NSString *pathDocuments  = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *albumsPath     = [NSString stringWithFormat:@"%@/%@",pathDocuments,kAlbums];
    return albumsPath;
}

- (BOOL)createFolderAtPath:(NSString *)folderPath {
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if (![[NSFileManager defaultManager] fileExistsAtPath:folderPath]) {
        [fileManager createDirectoryAtPath:folderPath withIntermediateDirectories:YES attributes:nil error:nil];
        return YES;
    }else{
        return NO;
    }
}

@end
