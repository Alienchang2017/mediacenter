//
//  AlbumModel.h
//  MediaCenter
//
//  Created by Alienchang on 2017/2/10.
//  Copyright © 2017年 Alienchang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AlbumModel : NSObject
@property (nonatomic ,copy)   NSString  *albumName;
@property (nonatomic ,copy)   NSString  *albumDescription;
@property (nonatomic ,copy)   NSString  *albumCover;
@property (nonatomic ,assign) NSInteger imagesCount;
@property (nonatomic ,strong) NSArray   *imagePaths;
@end
