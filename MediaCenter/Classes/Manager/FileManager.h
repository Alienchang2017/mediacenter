//
//  FileManager.h
//  MediaCenter
//
//  Created by Alienchang on 2016/12/5.
//  Copyright © 2016年 Alienchang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Photos/Photos.h>
#import "AlbumModel.h"
extern NSString * const kAlbums;
@interface FileManager : NSObject
+ (instancetype)manager;
- (void)createMediaFolder;
- (BOOL)writeImage:(UIImage *)image toPath:(NSString *)path;
- (BOOL)writeAsset:(PHAsset *)asset toPath:(NSString *)path;
- (BOOL)createAlbum:(AlbumModel *)albumModel;
- (NSArray <AlbumModel *>*)albums;
- (AlbumModel *)albumWithName:(NSString *)name;
@end
