//
//  MyAlbumsViewController.m
//  MediaCenter
//
//  Created by Alienchang on 2017/2/9.
//  Copyright © 2017年 Alienchang. All rights reserved.
//

#import "MyAlbumsViewController.h"
#import "FileManager.h"
#import "ImageHelper.h"
#import "MediaPickerViewController.h"
#import "MediaNavigationViewController.h"
#import "FileManager.h"
#import "AlbumTableViewCell.h"
#import "SafeObject.h"
@interface MyAlbumsViewController ()
@property (nonatomic ,strong) NSMutableArray <AlbumModel *>*albumModels;
@end

@implementation MyAlbumsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setTitle:@"相册"];
    [self.view setBackgroundColor:[UIColor colorOfGlobleBackgroundColor]];
    // Do any additional setup after loading the view.
    
    [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(addImageAction)]];
    [self.navigationItem setRightBarButtonItem:[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addAlbum)]];
    
    [self.albumModels addObjectsFromArray:[[FileManager manager] albums]];
    [self.tableView reloadData];
}

#pragma mark --private func
- (void)addAlbum{
    WS(weakSelf);
    SCLAlertView *alert = [SCLAlertView new];
    [alert setBackgroundType:SCLAlertViewBackgroundBlur];
    SCLTextView *textField = [alert addTextField:@"输入相册名"];
    alert.hideAnimationType = SCLAlertViewHideAnimationSimplyDisappear;
    [alert addButton:@"完成" validationBlock:^BOOL{
        if (!textField.text.length) {
//            [[[UIAlertView alloc] initWithTitle:nil message:@"请输入相册名" delegate:nil cancelButtonTitle:@"好的" otherButtonTitles:nil] show];
//            [textField becomeFirstResponder];
            return NO;
        }else{
            AlbumModel *albumModel = [AlbumModel new];
            [albumModel setAlbumName:textField.text];
            [[FileManager manager] createAlbum:albumModel];
        }
        return YES;
    } actionBlock:^{
        [weakSelf.albumModels addObject:[[FileManager manager] albumWithName:textField.text]];
        [weakSelf.tableView insertRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:weakSelf.albumModels.count - 1 inSection:0]] withRowAnimation:UITableViewRowAnimationLeft];
    }];
    [alert showEdit:self title:@"相册名" subTitle:nil closeButtonTitle:@"取消" duration:0];
}
- (void)addImageAction{
    [[FileManager manager] createMediaFolder];
    if (![ImageHelper isOpenAuthority]) {
        
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"您未打开照片权限" message:nil preferredStyle:UIAlertControllerStyleAlert];
        [alertController addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            ;
        }]];
        [alertController addAction:[UIAlertAction actionWithTitle:@"去设置" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [ImageHelper jumpToSetting];
        }]];
        
        [self presentViewController:alertController animated:YES completion:nil];
    }else{
        [ImageHelper getAlbumList:^(NSArray<PHFetchResult *> *albumList) {
            NSLog(@"albumList == %@",albumList);
            PHFetchResult *fetchResult = [albumList safeObjectAtIndex:0];
            NSLog(@"fetchResult == %@",fetchResult);
            if (fetchResult.count) {
                [ImageHelper getImageDataWithAsset:fetchResult[0] complete:^(UIImage *image) {
                    NSLog(@"image == %@",image);
                }];
            }
        }];
        
        MediaNavigationViewController *mediaNavigationViewController = [MediaNavigationViewController new];
        [self presentViewController:mediaNavigationViewController animated:YES completion:nil];
    }
}

#pragma mark --accoessor
- (NSMutableArray *)albumModels{
    if (!_albumModels) {
        _albumModels = [@[] mutableCopy];
    }
    return _albumModels;
}

#pragma mark --UITableViewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.albumModels.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    AlbumTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"test"];
    if (!cell) {
        cell = [[AlbumTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"test"];
    }
    
    AlbumModel *albumModel = self.albumModels[indexPath.row];
    [cell setupWithAlbumModel:albumModel];
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 90.;
}
@end
