//
//  AlbumTableViewCell.m
//  MediaCenter
//
//  Created by Alienchang on 2017/2/10.
//  Copyright © 2017年 Alienchang. All rights reserved.
//

#import "AlbumTableViewCell.h"
#import "AlbumImageView.h"
#import "UIImage+Common.h"
@interface AlbumTableViewCell()
@property (nonatomic ,strong) AlbumImageView *albumImageView;
@end
@implementation AlbumTableViewCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setBackgroundColor:[UIColor clearColor]];
        [self.textLabel setBackgroundColor:[UIColor clearColor]];
        [self setSelectionStyle:UITableViewCellSelectionStyleNone];
        [self.contentView.layer setCornerRadius:.5];
        [self.contentView.layer setMasksToBounds:YES];
        [self.contentView setBackgroundColor:[UIColor colorWithWhite:.6 alpha:.5]];
        [self.contentView addSubview:self.albumImageView];
        [self setNeedsUpdateConstraints];
    }
    return self;
}
- (void)setupWithAlbumModel:(AlbumModel *)albumModel{
    switch (rand() % 10) {
        case 1:
        {
            [self.albumImageView setImage:[UIImage imageNamed:@"GJVH3809.jpg"]];
        }
            break;
        case 2:
        {
            [self.albumImageView setImage:[UIImage imageNamed:@"Enlight1.jpg"]];
        }
            break;
        case 3:
        {
            [self.albumImageView setImage:[UIImage imageNamed:@"Enlight1 (2).jpg"]];
        }
            break;
        case 4:
        {
            [self.albumImageView setImage:[UIImage imageNamed:@"Enlight1 (1).jpg"]];
        }
            break;
        case 5:
        {
            [self.albumImageView setImage:[UIImage imageNamed:@"DSC04742-2.jpg"]];
        }
            break;
        case 6:
        {
            [self.albumImageView setImage:[UIImage imageNamed:@"IMG_6662.jpg"]];
        }
            break;
        case 7:
        {
            [self.albumImageView setImage:[UIImage imageNamed:@"DSC04630.jpg"]];
        }
            break;
            
        default:
        {
            [self.albumImageView setImage:[UIImage imageNamed:@"DSC03849.jpg"]];
        }
            break;
    }
    
}

#pragma mark --super func
- (void)updateConstraints{
    [super updateConstraints];
    [self.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.and.top.offset(5);
        make.right.and.bottom.offset(-5);
    }];
    
    [self.albumImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.and.top.offset(5);
        make.bottom.offset(-5);
        make.size.mas_equalTo(CGSizeMake(70, 70));
    }];
}

#pragma mark --accessor
- (AlbumImageView *)albumImageView{
    if (!_albumImageView) {
        _albumImageView = [AlbumImageView new];
        [_albumImageView setContentMode:UIViewContentModeScaleAspectFill];
        [_albumImageView.layer setMasksToBounds:YES];
    }
    return _albumImageView;
}
@end
