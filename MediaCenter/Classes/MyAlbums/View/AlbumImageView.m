//
//  AlbumImageView.m
//  MediaCenter
//
//  Created by Alienchang on 2017/2/10.
//  Copyright © 2017年 Alienchang. All rights reserved.
//

#import "AlbumImageView.h"

@implementation AlbumImageView

- (instancetype)init{
    self = [super init];
    if (self) {
        [self.layer setBorderWidth:2];
        [self.layer setBorderColor:[UIColor whiteColor].CGColor];
    }
    return self;
}

- (void)layoutSubviews{
    [super layoutSubviews];
    
}

@end
