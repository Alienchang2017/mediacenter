//
//  BaseNavigationViewController.m
//  MediaCenter
//
//  Created by Alienchang on 2017/2/9.
//  Copyright © 2017年 Alienchang. All rights reserved.
//

#import "BaseNavigationViewController.h"

@interface BaseNavigationViewController ()

@end

@implementation BaseNavigationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSDictionary * attributedTitleDic = @{
                                            NSFontAttributeName : [UIFont systemFontOfSize:20],
                                            NSForegroundColorAttributeName :[UIColor whiteColor],
                                            };
    
    [self.navigationBar setTitleTextAttributes:attributedTitleDic];
    [self.navigationBar setBarTintColor:[UIColor colorOfGlobleTint]];
    [self.navigationBar setTintColor:[UIColor whiteColor]];
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
