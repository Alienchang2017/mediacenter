//
//  MediaCollectionViewCell.h
//  MediaCenter
//
//  Created by Alienchang on 2016/12/5.
//  Copyright © 2016年 Alienchang. All rights reserved.
//

#import <UIKit/UIKit.h>
extern NSString *const kMediaCollectionViewCell;
@interface MediaCollectionViewCell : UICollectionViewCell
@property (nonatomic ,strong) UIImageView *imageView;
@property (nonatomic ,strong) UILabel     *titleLabel;
@property (nonatomic ,strong) NSIndexPath *indexPath;

@property (nonatomic, copy)   void (^longPressBlock)(void);

@end
