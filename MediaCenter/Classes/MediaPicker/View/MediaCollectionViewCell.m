//
//  MediaCollectionViewCell.m
//  MediaCenter
//
//  Created by Alienchang on 2016/12/5.
//  Copyright © 2016年 Alienchang. All rights reserved.
//

#import "MediaCollectionViewCell.h"
#import "Masonry.h"
#import "UIColor+Skin.h"
NSString *const kMediaCollectionViewCell = @"kMediaCollectionViewCell";
CGFloat  const  borderWidth = 3;
@interface MediaCollectionViewCell(){
    
}
@end
@implementation MediaCollectionViewCell

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self.contentView addSubview:self.imageView];
        [self.contentView addSubview:self.titleLabel];
        [self setNeedsUpdateConstraints];
    }
    return self;
}

- (void)updateConstraints{
    [super updateConstraints];
    __weak __typeof__(self) weakSelf = self;
    [self.imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(weakSelf);
    }];
}

#pragma mark --accessor
- (UIImageView *)imageView{
    if (!_imageView) {
        _imageView = [UIImageView new];
    }
    return _imageView;
}

- (UILabel *)titleLabel{
    if (!_titleLabel) {
        _titleLabel = [UILabel new];
        [_titleLabel setTextColor:[UIColor whiteColor]];
    }
    return _titleLabel;
}
- (void)setSelected:(BOOL)selected{
    [super setSelected:selected];
    if (selected) {
        [self.layer setBorderColor:[UIColor colorOfCellBorderColor].CGColor];
        [self.layer setBorderWidth:borderWidth];
    }else{
        [self.layer setBorderColor:[UIColor clearColor].CGColor];
        [self.layer setBorderWidth:0];
    }
}
#pragma mark --private func
- (void)longPressAction{
    self.longPressBlock?self.longPressBlock(): nil;
}
@end
