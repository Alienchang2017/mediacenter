//
//  AlbumCollectionViewCell.m
//  MediaCenter
//
//  Created by Alienchang on 2016/12/7.
//  Copyright © 2016年 Alienchang. All rights reserved.
//

#import "AlbumCollectionViewCell.h"
#import "Masonry.h"
@interface AlbumCollectionViewCell()
@property (nonatomic ,strong) UIView *maskView;
@end
NSString *const kAlbumCollectionViewCell = @"kAlbumCollectionViewCell";

@implementation AlbumCollectionViewCell
- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self.contentView addSubview:self.imageView];
        [self.contentView addSubview:self.titleLabel];
        [self.contentView addSubview:self.maskView];
        [self.imageView.layer setMasksToBounds:YES];
        [self setNeedsUpdateConstraints];
    }
    return self;
}

- (void)updateConstraints{
    [super updateConstraints];
    __weak __typeof__(self) weakSelf = self;
    [self.imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.and.right.and.top.offset(0);
        make.height.equalTo(weakSelf.mas_width);
    }];
    
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(weakSelf.imageView.mas_bottom).offset(5);
        make.left.and.right.offset(0);
    }];
    
    [self.maskView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.and.right.and.top.offset(0);
        make.height.equalTo(weakSelf.mas_width);
    }];
}

#pragma mark --accessor
- (UIView *)maskView{
    if (!_maskView) {
        _maskView = [UIView new];
        [_maskView setBackgroundColor:[UIColor colorWithWhite:.2 alpha:.5]];
    }
    return _maskView;
}
- (UIImageView *)imageView{
    if (!_imageView) {
        _imageView = [UIImageView new];
        [_imageView setContentMode:UIViewContentModeScaleAspectFill];
    }
    return _imageView;
}

- (UILabel *)titleLabel{
    if (!_titleLabel) {
        _titleLabel = [UILabel new];
        [_titleLabel setTextColor:[UIColor darkGrayColor]];
        [_titleLabel setTextAlignment:NSTextAlignmentCenter];
    }
    return _titleLabel;
}

- (void)setSelected:(BOOL)selected{
    [super setSelected:selected];
    [self.titleLabel setTextColor:selected?[UIColor whiteColor]:[UIColor darkGrayColor]];
    [self.maskView setBackgroundColor:selected?[UIColor clearColor]:[UIColor colorWithWhite:.2 alpha:.5]];
}
@end
