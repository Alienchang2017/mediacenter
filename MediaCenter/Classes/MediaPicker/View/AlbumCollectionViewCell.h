//
//  AlbumCollectionViewCell.h
//  MediaCenter
//
//  Created by Alienchang on 2016/12/7.
//  Copyright © 2016年 Alienchang. All rights reserved.
//

#import <UIKit/UIKit.h>
extern NSString *const kAlbumCollectionViewCell;
@interface AlbumCollectionViewCell : UICollectionViewCell
@property (nonatomic ,strong) UIImageView *imageView;
@property (nonatomic ,strong) UILabel     *titleLabel;
@end
