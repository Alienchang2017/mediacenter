//
//  AssetsLibraryManager.m
//  MediaCenter
//
//  Created by Alienchang on 2016/12/5.
//  Copyright © 2016年 Alienchang. All rights reserved.
//

#import "ImageSourceManager.h"
#import <Photos/Photos.h>

@implementation ImageSourceManager
+ (ImageSourceManager *)shareManager
{
    static ImageSourceManager *sharedManager = nil;
    static dispatch_once_t predicate;
    dispatch_once(&predicate, ^{
        sharedManager = [[self alloc] init];
    });
    return sharedManager;
}



@end
