//
//  MediaPickerViewController.m
//  MediaCenter
//
//  Created by Alienchang on 2016/12/5.
//  Copyright © 2016年 Alienchang. All rights reserved.
//

#import "MediaPickerViewController.h"
#import "ImageHelper.h"
#import "MediaCollectionViewCell.h"
#import "Masonry.h"
#import "IDMPhotoBrowser.h"
#import "FileManager.h"
#import "AlbumCollectionViewCell.h"
@interface MediaPickerViewController ()<IDMPhotoBrowserDelegate ,UICollectionViewDelegate ,UICollectionViewDataSource ,UICollectionViewDelegateFlowLayout ,UIGestureRecognizerDelegate>{
    CGFloat _screenWidth;
    CGFloat _screenHeight;
    CGFloat _itemWidth;
    CGFloat _padding;
    CGFloat _maxItemHeight;
}
@property (nonatomic ,strong) NSMutableArray   *albumList;
@property (nonatomic ,strong) UICollectionView *albumsCollectionView;
@property (nonatomic ,strong) UICollectionView *mediasCollectionView;
@property (nonatomic ,strong) PHFetchResult    *fetchResult;
@property (nonatomic ,strong) UINavigationBar  *navigationBar;
;

@property (nonatomic ,strong) NSMutableArray   <PHAsset *>*selectedAssets;


@end

@implementation MediaPickerViewController
- (instancetype)init
{
    self = [super init];
    if (self) {
        self->_padding     = 15.;
        self->_screenWidth = [UIScreen mainScreen].bounds.size.width;
        self->_screenHeight= [UIScreen mainScreen].bounds.size.height;
//        self->_itemWidth   = (_screenWidth - 4 * _padding) / 3 - 2;
        self->_itemWidth   = (_screenWidth - 4 * _padding) / 3;
        self->_maxItemHeight = 200.;
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view addSubview:self.mediasCollectionView];
    
    
    [self.mediasCollectionView setBackgroundColor:[UIColor blackColor]];

    [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:@"返回" style:UIBarButtonItemStylePlain target:self action:@selector(backAction)]];
    [self.navigationItem setRightBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:@"导入" style:UIBarButtonItemStylePlain target:self action:@selector(rightBarButtonAction)]];
    [self.navigationItem.rightBarButtonItem setEnabled:NO];
    
    __weak __typeof__(self) weakSelf = self;
    [ImageHelper getAlbumList:^(NSArray<PHFetchResult *> *albumList) {
        [weakSelf.albumList addObjectsFromArray:albumList];
        [weakSelf.mediasCollectionView reloadData];
    }];
    [self updateViewConstraints];
}

- (void)updateViewConstraints{
    [super updateViewConstraints];
    [self.mediasCollectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.offset(0);
    }];
}
#pragma mark --accessor
- (NSMutableArray *)selectedAssets{
    if (!_selectedAssets) {
        _selectedAssets = [NSMutableArray new];
    }
    return _selectedAssets;
}

- (NSMutableArray *)albumList{
    if (!_albumList) {
        _albumList = [NSMutableArray new];
    }
    return _albumList;
}
- (PHFetchResult *)fetchResult{
    if (!_fetchResult) {
        _fetchResult = self.albumList[0];
    }
    return _fetchResult;
}

- (UICollectionView *)mediasCollectionView{
    if (!_mediasCollectionView) {
        UICollectionViewFlowLayout *itemFlowLayout = [UICollectionViewFlowLayout new];
        itemFlowLayout.sectionInset = UIEdgeInsetsMake(self->_padding, self->_padding, self->_padding, self->_padding);
        itemFlowLayout.minimumInteritemSpacing = self->_padding;
        itemFlowLayout.minimumLineSpacing = self->_padding;
        
        _mediasCollectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:itemFlowLayout];
        UILongPressGestureRecognizer *gestureLongPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(gestureLongPress:)];
        [gestureLongPress setMinimumPressDuration:1];
        UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(doubleTapAction:)];
        [tapGestureRecognizer setNumberOfTapsRequired:2];
        [_mediasCollectionView addGestureRecognizer:tapGestureRecognizer];
        [_mediasCollectionView addGestureRecognizer:gestureLongPress];
        [_mediasCollectionView setAllowsMultipleSelection:YES];
        [_mediasCollectionView registerClass:[MediaCollectionViewCell class] forCellWithReuseIdentifier:kMediaCollectionViewCell];
        [_mediasCollectionView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"albumsCollectionView"];
        [_mediasCollectionView setShowsVerticalScrollIndicator:NO];
        [_mediasCollectionView setShowsHorizontalScrollIndicator:NO];
        [_mediasCollectionView setDataSource:self];
        [_mediasCollectionView setDelegate:self];
    }
    return _mediasCollectionView;
}
- (UICollectionView *)albumsCollectionView{
    if (!_albumsCollectionView) {
        UICollectionViewFlowLayout *itemFlowLayout = [UICollectionViewFlowLayout new];
        [itemFlowLayout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
        itemFlowLayout.sectionInset = UIEdgeInsetsMake(self->_padding, self->_padding, self->_padding, self->_padding);
        itemFlowLayout.minimumInteritemSpacing = self->_padding;
        itemFlowLayout.minimumLineSpacing = self->_padding;
        _albumsCollectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:itemFlowLayout];
        [_albumsCollectionView registerClass:[AlbumCollectionViewCell class] forCellWithReuseIdentifier:kAlbumCollectionViewCell];
        [_albumsCollectionView setShowsVerticalScrollIndicator:NO];
        [_albumsCollectionView setShowsHorizontalScrollIndicator:NO];
        [_albumsCollectionView setDataSource:self];
        [_albumsCollectionView setDelegate:self];
        [_albumsCollectionView selectItemAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:NO scrollPosition:UICollectionViewScrollPositionNone];
    }
    return _albumsCollectionView;
}
#pragma mark --UICollectionViewDelegate
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if (collectionView == self.albumsCollectionView) {
        return ((PHFetchResult *)self.albumList[1]).count + 1;
    }else{
        return self.fetchResult.count;
    }
}
- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (collectionView == self.albumsCollectionView) {
        AlbumCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kAlbumCollectionViewCell forIndexPath:indexPath];
        if (indexPath.row == 0) {
            PHFetchResult *fetchResult = self.albumList[0];
            if (fetchResult.count > 0) {
                PHAsset *asset  = fetchResult[0];
                CGSize size     = [self caculateItemSizeWithAsset:asset];
                
                __block __typeof__(cell)blockCell = cell;
                [ImageHelper getImageWithAsset:asset targetSize:CGSizeMake(size.width * 2, size.height * 2) complete:^(UIImage *image) {
                    [blockCell.imageView setImage:image];
                    [blockCell.titleLabel setText:@"相机相册"];
                }];
                return cell;
            }
            return cell;
        }else{
            NSArray *assetCollections = self.albumList[1];
            PHAssetCollection *assetCollection = assetCollections[indexPath.row - 1];
            
            PHFetchResult *fetchResult = [PHAsset fetchAssetsInAssetCollection:assetCollection options:nil];
            
            if (fetchResult.count > 0) {
                PHAsset *asset  = fetchResult[0];
                CGSize size     = [self caculateItemSizeWithAsset:asset];
                
                __block __typeof__(cell)blockCell = cell;
                [ImageHelper getImageWithAsset:asset targetSize:CGSizeMake(size.width * 2, size.height * 2) complete:^(UIImage *image) {
                    [blockCell.imageView setImage:image];
                    [blockCell.titleLabel setText:assetCollection.localizedTitle];
                }];
                return cell;
            }
            return cell;
        }
        
    }else{
        MediaCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kMediaCollectionViewCell forIndexPath:indexPath];
        PHAsset *asset  = self.fetchResult[indexPath.row];
        CGSize size     = [self caculateItemSizeWithAsset:asset];
        [cell setIndexPath:indexPath];
        __block __typeof__(cell)blockCell = cell;
        [ImageHelper getImageWithAsset:self.fetchResult[indexPath.row] targetSize:CGSizeMake(size.width * 2, size.height * 2) complete:^(UIImage *image) {
            if ([cell.indexPath isEqual:indexPath]) {
                [blockCell.imageView setImage:image];
            }
        }];
        return cell;
    }
    
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if (collectionView == self.albumsCollectionView) {
        if (indexPath.row == 0) {
            [self setFetchResult:self.albumList[0]];
        }else{
            NSArray *assetCollections = self.albumList[1];
            PHAssetCollection *assetCollection = assetCollections[indexPath.row - 1];
            PHFetchResult *fetchResult = [PHAsset fetchAssetsInAssetCollection:assetCollection options:nil];
            [self setFetchResult:fetchResult];
        }
        [self.mediasCollectionView reloadData];
        
        
    }else{
        PHAsset *asset  = self.fetchResult[indexPath.row];
        [self.selectedAssets addObject:asset];
        [self.navigationItem.rightBarButtonItem setEnabled:YES];
    }
}
- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath{
    if (collectionView == self.albumsCollectionView) {
        
    }else{
        PHAsset *asset  = self.fetchResult[indexPath.row];
        [self.selectedAssets removeObject:asset];
        
        if (!self.selectedAssets.count) {
            [self.navigationItem.rightBarButtonItem setEnabled:NO];
        }
    }
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (collectionView == self.albumsCollectionView) {
        return CGSizeMake(self->_itemWidth, self->_itemWidth + 30);
    }else{
        PHAsset *asset = self.fetchResult[indexPath.row];
        return [self caculateItemSizeWithAsset:asset];
    }
}


- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView
           viewForSupplementaryElementOfKind:(NSString *)kind
                                 atIndexPath:(NSIndexPath *)indexPath{
    if (collectionView == self.albumsCollectionView) {
        return [UICollectionReusableView new];
    }else{
        UICollectionReusableView *collectionReusableView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"albumsCollectionView" forIndexPath:indexPath];
        [collectionReusableView addSubview:self.albumsCollectionView];
        [self.albumsCollectionView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.offset(0);
        }];
        return collectionReusableView;
    }
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section{
    
    if (collectionView == self.albumsCollectionView) {
        return CGSizeZero;
    }else{
        return CGSizeMake(kScreenWidth, 200);
    }
}

#pragma mark --private func
- (CGSize)caculateItemSizeWithAsset:(PHAsset *)asset{
    CGFloat expectWidth  = self->_itemWidth;
    CGFloat expectHeight = self->_itemWidth /asset.pixelWidth *asset.pixelHeight;
    if (expectHeight > self->_maxItemHeight) {
        expectWidth  = self->_maxItemHeight / expectHeight * self->_itemWidth;
        expectHeight = self->_maxItemHeight;
    }
    CGSize size = CGSizeMake(expectWidth, expectHeight);
    return size;
}

- (void)rightBarButtonAction{
    for (PHAsset *asset in self.selectedAssets) {
        [[FileManager manager] writeAsset:asset toPath:@"test"];
    }
}
- (void)backAction{
    if (self.presentingViewController) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}
- (void)gestureLongPress:(UILongPressGestureRecognizer *)gestureRecognizer
{
    CGPoint touchPoint = [gestureRecognizer locationInView:self.mediasCollectionView];
    if (gestureRecognizer.state == UIGestureRecognizerStateBegan) {
        
        NSIndexPath *indexPath = [self.mediasCollectionView indexPathForItemAtPoint:touchPoint];
        if (indexPath == nil) {
            NSLog(@"not cell");
        }else{
            PHAsset *asset  = self.fetchResult[indexPath.row];
            
            [ImageHelper getImageDataWithAsset:asset complete:^(UIImage *image) {
                IDMPhoto        *photo   = [IDMPhoto photoWithImage:image];
                IDMPhotoBrowser *browser = [[IDMPhotoBrowser alloc] initWithPhotos:@[photo]];
                
                browser.displayCounterLabel     = NO;
                browser.displayActionButton     = NO;
                browser.displayDoneButton       = NO;
                browser.displayToolbar          = NO;
                
                [self presentViewController:browser animated:YES completion:nil];
            }];
        }
    }
}

- (void)doubleTapAction:(UITapGestureRecognizer *)gestureRecognizer{
    CGPoint touchPoint = [gestureRecognizer locationInView:self.mediasCollectionView];
    if (gestureRecognizer.state == UIGestureRecognizerStateEnded) {
        
        NSIndexPath *indexPath = [self.mediasCollectionView indexPathForItemAtPoint:touchPoint];
        if (indexPath == nil) {
            NSLog(@"not cell");
        }else{
            PHAsset *asset  = self.fetchResult[indexPath.row];
            
            [ImageHelper getImageDataWithAsset:asset complete:^(UIImage *image) {
                IDMPhoto        *photo   = [IDMPhoto photoWithImage:image];
                IDMPhotoBrowser *browser = [[IDMPhotoBrowser alloc] initWithPhotos:@[photo]];
                
                browser.displayCounterLabel     = NO;
                browser.displayActionButton     = NO;
                browser.displayDoneButton       = NO;
                browser.displayToolbar          = NO;
                
                [self presentViewController:browser animated:YES completion:nil];
            }];
        }
    }
}
@end
