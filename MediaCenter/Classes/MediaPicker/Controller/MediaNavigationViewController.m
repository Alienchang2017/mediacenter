//
//  MediaNavigationViewController.m
//  MediaCenter
//
//  Created by Alienchang on 2016/12/6.
//  Copyright © 2016年 Alienchang. All rights reserved.
//

#import "MediaNavigationViewController.h"
#import "MediaPickerViewController.h"
@interface MediaNavigationViewController ()

@end

@implementation MediaNavigationViewController
- (instancetype)init
{
    self = [super initWithRootViewController:[MediaPickerViewController new]];
    if (self) {
        
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationBar setBarTintColor:[UIColor blackColor]];
    [self.navigationBar setTintColor:[UIColor whiteColor]];
    [self.navigationBar setTranslucent:NO];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
