//
//  TestIGViewController.m
//  MediaCenter
//
//  Created by Alienchang on 2016/12/6.
//  Copyright © 2016年 Alienchang. All rights reserved.
//

#import "TestIGViewController.h"
#import "IGListKit.h"
@interface TestIGViewController ()<IGListAdapterDataSource>
@property (nonatomic ,strong) IGListAdapter    *listAdapter;
@property (nonatomic ,strong) IGListCollectionView *igCollectionView;
@end

@implementation TestIGViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark --accessor
- (IGListAdapter *)listAdapter{
    if (!_listAdapter) {
        _listAdapter = [[IGListAdapter alloc] initWithUpdater:[IGListAdapterUpdater new] viewController:self workingRangeSize:0];
        [_listAdapter setDataSource:self];
        [_listAdapter setCollectionView:self.igCollectionView];
    }
    return _listAdapter;
}

- (IGListCollectionView *)igCollectionView{
    if (!_igCollectionView) {
        UICollectionViewFlowLayout *itemFlowLayout = [UICollectionViewFlowLayout new];
//        itemFlowLayout.sectionInset = UIEdgeInsetsMake(self->_padding, self->_padding, self->_padding, self->_padding);
//        itemFlowLayout.minimumInteritemSpacing = self->_padding;
//        itemFlowLayout.minimumLineSpacing = self->_padding;
        _igCollectionView = [[IGListCollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:itemFlowLayout];
    }
    return _igCollectionView;
}
#pragma mark --IGListAdapterDataSource
- (NSArray<id <IGListDiffable>> *)objectsForListAdapter:(IGListAdapter *)listAdapter{
    return nil;
}

- (IGListSectionController <IGListSectionType> *)listAdapter:(IGListAdapter *)listAdapter sectionControllerForObject:(id)object{
    return nil;
}

@end
