//
//  NSDictionary+Extension.h
//  MediaCenter
//
//  Created by Alienchang on 2017/2/10.
//  Copyright © 2017年 Alienchang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (Extension)
+ (NSDictionary *)dictionaryWithJsonString:(NSString *)jsonString;
+ (NSString *)convertToJSONData:(id)infoDic;
@end
