//
//  UIColor+Skin.h
//  WKCarInsurance
//
//  Created by Alienchang on 2016/11/25.
//  Copyright © 2016年 wkbins. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (Skin)
+ (UIColor *)testColor;
+ (UIColor *)colorOfNegative;
+ (UIColor *)colorOfPositive;
+ (UIColor *)colorOfCellBorderColor;
+ (UIColor *)colorOfGlobleTint;
+ (UIColor *)colorOfGlobleBackgroundColor;
@end
