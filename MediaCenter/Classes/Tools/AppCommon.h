//
//  AppConst.h
//  WKCarInsurance
//
//  Created by Alienchang on 2016/11/26.
//  Copyright © 2016年 wkbins. All rights reserved.
//
#import <Foundation/Foundation.h>
typedef void(^FailureBlock)(NSError *error);
typedef void(^SuccessBlock)(id json);

#define SUCCESSCODE 200

#define WS(weakSelf)  __weak __typeof(&*self)weakSelf = self;
#define SCREEN_WIDTH  ([UIScreen mainScreen].bounds.size.width)
#define SCREEN_HEIGHT ([UIScreen mainScreen].bounds.size.height)
#define Rect(x,y,w,h) CGRectMake(x,y,w,h)



#ifndef danbai_client_ios_DBPhone_h
#define danbai_client_ios_DBPhone_h


/** 设备型号识别 */
#define iOS8 ([[UIDevice currentDevice].systemVersion doubleValue] >= 8.0) && [[UIDevice currentDevice].systemVersion doubleValue] < 9.0)
#define iOS7 ([[UIDevice currentDevice].systemVersion doubleValue] >= 7.0 && [[UIDevice currentDevice].systemVersion doubleValue] < 8.0)

/** 尺寸 */
#define iphone4And4s ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(640, 960),[[UIScreen mainScreen] currentMode].size) : NO)

#define iPhone5And5s ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(640, 1136), [[UIScreen mainScreen] currentMode].size) : NO)

#define iPhone6And6s ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? (CGSizeEqualToSize(CGSizeMake(750, 1334), [[UIScreen mainScreen] currentMode].size) || CGSizeEqualToSize(CGSizeMake(640, 1136), [[UIScreen mainScreen] currentMode].size)) : NO)

#define iPhone6plusAnd6splus ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? (CGSizeEqualToSize(CGSizeMake(1125, 2001), [[UIScreen mainScreen] currentMode].size) || CGSizeEqualToSize(CGSizeMake(1242, 2208), [[UIScreen mainScreen] currentMode].size)) : NO)

#endif
