//
//  UIColor+Skin.m
//  WKCarInsurance
//
//  Created by Alienchang on 2016/11/25.
//  Copyright © 2016年 wkbins. All rights reserved.
//

#import "UIColor+Skin.h"
#import "HexColors.h"
@implementation UIColor (Skin)
+ (UIColor *)testColor{
    return [HXColor colorWithHexString:@"#1c1c1c"];
}
+ (UIColor *)colorOfNegative{
    return [HXColor colorWithHexString:@"#ffb049"];
}
+ (UIColor *)colorOfPositive{
    return [HXColor colorWithHexString:@"#3987ff"];
}
+ (UIColor *)colorOfCellBorderColor{
    return [HXColor colorWithRed:209 / 255. green:184 / 255. blue:140 / 255. alpha:1];
}
+ (UIColor *)colorOfGlobleTint{
    return [HXColor colorWithRed:84 / 255. green:74 / 255. blue:139 / 255. alpha:1];
}
+ (UIColor *)colorOfGlobleBackgroundColor{
    return [HXColor colorWithRed:65 / 255. green:55 / 255. blue:127 / 255. alpha:1];
}
@end
