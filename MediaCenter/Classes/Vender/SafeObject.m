//
//  SafeObject.m
//  Acquirer
//
//  Created by chinaPnr on 13-10-24.
//  Copyright (c) 2013年 chinaPnr. All rights reserved.
//

#import "SafeObject.h"

@implementation NSArray(SafeObject)

- (id)safeObjectAtIndex:(NSUInteger)index
{
    if(self.count == 0) {
        //printf("\n--- array have no objects ---\n");
        return (nil);
    }
    if(index > MAX(self.count - 1, 0)) {
        //printf("\n--- index:%li out of array range ---\n", (long)index);
        return (nil);
    }
    return ([self objectAtIndex:index]);
}

@end

//-----------------------------------------------------
//-----------------------------------------------------

@implementation NSMutableArray(SafeObject)

- (void)safeAddObject:(id)anObject
{
    if(!anObject) {
        return;
    }
    [self addObject:anObject];
}

- (void)safeInsertObject:(id)anObject atIndex:(NSUInteger)index
{
    if(index > MAX(self.count - 1, 0)) {
        return;
    }
    if(!anObject) {
        
        return;
    }
    [self insertObject:anObject atIndex:index];
}

- (void)safeRemoveObjectAtIndex:(NSUInteger)index
{
    if(index > MAX(self.count - 1, 0)) {
        
        return;
    }
    [self removeObjectAtIndex:index];
}

- (void)safeReplaceObjectAtIndex:(NSUInteger)index withObject:(id)anObject
{
    if(index > MAX(self.count - 1, 0)) {
        
        return;
    }
    if(!anObject) {
        
        return;
    }
    [self replaceObjectAtIndex:index withObject:anObject];
}

@end


#define checkNull(__X__) (__X__) == [NSNull null] || (__X__) == nil ? @"" : [NSString stringWithFormat:@"%@", (__X__)]

@implementation NSDictionary(SafeObject)

- (NSString *)stringObjectForKey:(id <NSCopying>)key {
    id ob = [self objectForKey:key];
    if(ob == [NSNull null] || ob == nil) {
        return (@"");
    }
    if([ob isKindOfClass:[NSString class]]) {
        return (ob);
    }
    return ([NSString stringWithFormat:@"%@", ob]);
}

- (id)safeJsonObjectForKey:(id <NSCopying>)key {
    id ob = [self objectForKey:key];
    if(ob == [NSNull null]) {
        return (nil);
    }
    return (ob);
}

@end


@implementation NSMutableDictionary(SafeObject)

- (void)safeSetObject:(id)anObject forKey:(id <NSCopying>)aKey {
    if(!aKey || !anObject) {
        
    } else {
        [self setObject:anObject forKey:aKey];
    }
}

@end

